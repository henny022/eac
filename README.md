# EAC
Good morning, <span style="color:green">Master Smith</span>.

EAC compiles C patches for the use with [Event Assembler](https://feuniverse.us/t/event-assembler/1749). 
It is made to be used with decompilation projects like [zeldaret/tmc](https://github.com/zeldaret/tmc/).

## WIP Warning
This project is still very much in early development.
It was made for TMC, so it is only tested with TMC and that what the examples are made with.

## Example usage
The example assumes you are creating a hack for the EU version of TMC.
It should be easy enough to adapt for any other game, given you have a `.elf` file of the game (referenced for symbols, usually requires decompilation of the game).

- grab [ColorzCore](https://github.com/FireEmblemUniverse/ColorzCore/blob/master/ColorzCore/bin/Release/ColorzCore.exe)
- grab some `Language Raws` (from EA or I use the ones provided with [TMCR](https://github.com/minishmaker/randomizer/tree/master/Vendor/Language%20Raws), required are `BYTE`, `SHORT`, `WORD` and `POIN`)
- configure the `Makefile` (requires a fully setup and build copy of the [zeldaret/tmc](https://github.com/zeldaret/tmc/) repo, target `eu`)
- install `eac` using `pip install .` (some kind of virtual environment recommended)
- run `make run` or whatever other target you want to test (inside the virtual environment if present)

## General usage
- install `eac` using `pip install git+http://gitlab.com/henny022/eac.git` (virtual environment recommended)
- extract game symbols from game `.elf` file using `eac extract -o symbols.json game.elf`
- compile you C patch into an object file
- create EA patch using `eac compile --symbols symbols.json -o patch.event patch.o`
- build your Hack using EA

## Credit
Shoutouts to the following people
- LeonarthCG for being and awesome help with EA in general
- StanH and [this post](https://feuniverse.us/t/guide-doc-asm-hacking-in-c-with-ea/3351) on a very similar project was a lot of help

## License
This is licensed under The Unlicense so do whatever you want.
Even if not required, if you can give credit please do so.
