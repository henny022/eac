import json


def main(args):
    with open(args.symbols) as game_symbols:
        game_symbols = json.load(game_symbols)
    with open(args.output, 'w') as outfile:
        print('#define SYMBOL_OFFSET(symbol) (symbol&0x1ffffff)', file=outfile)
        print('#define FUNCTION_OFFSET(symbol) (symbol&0x1fffffe)', file=outfile)
        for category in game_symbols:
            print(f'// {category} symbols', file=outfile)
            for symbol in game_symbols[category]:
                if not symbol:
                    continue
                print(f'#define {symbol} {game_symbols[category][symbol]["value"]:#x}', file=outfile)
