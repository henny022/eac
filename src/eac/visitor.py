from .ast import *


class Visitor:
    def visit(self, node):
        name = node.__class__.__name__.lower()
        return getattr(self, f'visit_{name}')(node)

    def visit_patch(self, patch: Patch):
        return [self.visit(x) for x in patch.text], [self.visit(x) for x in patch.rodata]
