from dataclasses import dataclass
from typing import Union


@dataclass
class OP:
    value: int


@dataclass
class BL:
    target_name: str
    target_value: int = None


@dataclass
class Abs32:
    target_name: str
    value_offset: int = None
    target_value: int = None


@dataclass
class RawData:
    data: bytes


@dataclass
class DataBlock:
    name: str
    content: list[Union[RawData, Abs32]]
    size: int
    location: int = None
    space: int = None


@dataclass
class TextBlock:
    name: str
    content: list
    size: int
    thumb: bool
    location: int = None
    space: int = None


@dataclass
class Patch:
    text: list[TextBlock]
    rodata: list[DataBlock]
