import json

from elftools.elf.elffile import ELFFile


def main(args):
    symbols = get_symbols(args.input)
    with open(args.output, 'w') as output:
        json.dump(symbols, output, indent=2)


def get_symbols(path):
    with open(f'{path}', 'rb') as elffile:
        elf = ELFFile(elffile)
        symbol_table = elf.get_section_by_name('.symtab')
        symbols = {
            'functions': {},
            'objects': {},
            'other': {},
        }
        for symbol in symbol_table.iter_symbols():
            type = symbol.entry.st_info.type
            if type in ['STT_SECTION', 'STT_FILE']:
                # skip these, no need for them
                pass
            elif type == 'STT_FUNC':
                symbols['functions'][symbol.name] = {
                    'value': symbol.entry.st_value,
                    'size': symbol.entry.st_size,
                }
            elif type == 'STT_OBJECT':
                symbols['objects'][symbol.name] = {
                    'value': symbol.entry.st_value,
                    'size': symbol.entry.st_size,
                }
            elif type == 'STT_NOTYPE':
                symbols['other'][symbol.name] = {
                    'value': symbol.entry.st_value,
                }
            else:
                print(f'unknown symbol type {type}')
    return symbols
