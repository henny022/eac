import json
import struct

import elftools.elf.relocation
import elftools.elf.sections
from elftools.elf.elffile import ELFFile

from .ast import *
from .dump_ea import DumpEA

standalone_ea_header = '''
#define jumpToHack(offset) "BYTE 0x00 0x4B 0x18 0x47; POIN (offset|0x1)"

#define BLRange(pointer) "((pointer - (CURRENTOFFSET + 4))>>1)"
#define BL(pointer) "SHORT (((BLRange(pointer)>>11)&0x7ff)|0xf000) ((BLRange(pointer)&0x7ff)|0xf800);"

// function call veneer
// veneer:
//   push {r3}
//   ldr r3, _function
//   mov ip, r3
//   pop {r3}
//   bx ip
//   nop
// _function:
//   .word 0
#define veneer(offset) "SHORT 0xB408 0x4B02 0x469C 0xBC08 0x4760; ALIGN 4; POIN (offset|0x1)"

#define FreeSpace 0xEF3340
ORG FreeSpace

'''


class GameSymbols:
    def __init__(self, data):
        self.data = data

    def get_symbol(self, name):
        if name in self.data['objects']:
            return self.data['objects'][name]
        if name in self.data['other']:
            return self.data['other'][name]
        if name in self.data['functions']:
            return self.data['functions'][name]
        return None

    def get_function(self, name):
        if name in self.data['functions']:
            return self.data['functions'][name]
        return None


def main(args):
    with open(args.symbols) as game_symbols:
        game_symbols = GameSymbols(json.load(game_symbols))
    elf = ELF(args.input)
    patch = process(elf, game_symbols)
    with open(args.output, 'w') as eventfile:
        if args.standalone:
            print(standalone_ea_header, file=eventfile)
        print('ALIGN 4', file=eventfile)
        DumpEA(eventfile, args.input, args.allow_unsafe_hooks).visit(patch)


class Symbol:
    value: int
    name: str
    size: int
    type: str
    scope: str
    section: str

    TYPE_FUNCTION = 'FUNC'
    TYPE_SECTION = 'SECTION'

    def __init__(self, symbol: elftools.elf.sections.Symbol, section_names: list[str]):
        self.value = symbol.entry.st_value
        self.name = symbol.name
        self.size = symbol.entry.st_size
        self.type = symbol.entry.st_info.type[4:]
        self.scope = symbol.entry.st_info.bind[4:]
        if isinstance(symbol.entry.st_shndx, str):
            self.section = symbol.entry.st_shndx
        else:
            self.section = section_names[symbol.entry.st_shndx]

    @property
    def offset(self):
        return self.value & ~1

    def __repr__(self):
        return f'Symbol({self.name})'


class Relocation:
    offset: int
    type: int
    target: str

    def __init__(self, relocation: elftools.elf.relocation.Relocation, symbols: list[Symbol],
                 section_symbols: dict[str, dict[int, Symbol]]):
        self.offset = relocation.entry.r_offset
        self.type = relocation.entry.r_info_type
        assert self.type in [2, 10], f'unknown relocation type {self.type}'
        target = symbols[relocation.entry.r_info_sym]
        if target.type == Symbol.TYPE_SECTION:
            if target.section in section_symbols and 0 in section_symbols[target.section]:
                self.target = section_symbols[target.section][0].name
            else:
                self.target = target.section
        else:
            self.target = target.name

    def __repr__(self):
        return f'Relocation({self.target}, {self.type})'


class Section:
    name: str
    data: bytes
    symbols: dict[int, Symbol]
    relocations: dict[int, Relocation]

    def __init__(
            self,
            section,
            relocations: elftools.elf.relocation.RelocationSection,
            symbols: list[Symbol],
            section_symbols: dict[str, dict[int, Symbol]]
    ):
        self.name = section.name
        self.data = section.data()
        if section.name in section_symbols:
            self.symbols = {
                offset: symbol
                for offset, symbol in section_symbols[section.name].items()
                if symbol.size > 0
            }
        else:
            self.symbols = {}
        if relocations is None:
            self.relocations = {}
        else:
            self.relocations = {
                relocation.entry.r_offset: Relocation(relocation, symbols, section_symbols)
                for relocation in relocations.iter_relocations()
                # 40 is R_ARM_V4BX, which is a nop relocation in our case
                if relocation.entry.r_info_type not in [40]
            }
        pass

    def byte(self, offset):
        return struct.unpack('<B', self.data[offset:offset + 1])[0]

    def short(self, offset):
        return struct.unpack('<H', self.data[offset:offset + 2])[0]

    def word(self, offset, signed=False):
        if signed:
            return struct.unpack('<i', self.data[offset:offset + 4])[0]
        return struct.unpack('<I', self.data[offset:offset + 4])[0]

    @property
    def size(self):
        return len(self.data)

    @property
    def symbol_names(self):
        return [x.name for x in self.symbols.values()]

    def __repr__(self):
        return f'Section({self.name})'


class ELF:
    def __init__(self, filename):
        with open(filename, 'rb') as obj:
            elf = ELFFile(obj)
            section_names = [section.name for section in elf.iter_sections()]
            symbols = [
                Symbol(x, section_names)
                for x in elf.get_section_by_name('.symtab').iter_symbols()
            ]
            section_symbols = {}
            for symbol in symbols:
                if symbol.name.startswith('$'):
                    continue
                if not symbol.name:
                    continue
                if symbol.section not in section_symbols:
                    section_symbols[symbol.section] = {}
                section_symbols[symbol.section][symbol.offset] = symbol
            self.text, self.rodata = [
                [
                    Section(elf.get_section_by_name(name), elf.get_section_by_name(f'.rel{name}'), symbols,
                            section_symbols)
                    for name in section_names
                    if name.startswith(f'{base}.')
                ]
                for base in ['.text', '.rodata']
            ]
            print('ok')


def get_sections(base: str, sections: dict, symbols: list[Symbol]):
    s = {
        section.name[len(base) + 1:]: (
            section,
            sections.get(f'.rel{section.name}'),
            {
                x.offset: x
                for x in symbols
                if x.size != 0 and sections[x.section].name == section.name
            },
        )
        for section in sections.values()
        if section.name.startswith(base)
    }

    if '' in s:
        assert len(s[''][0].data()) == 0, f'section {base} is not empty'
        del s['']
    return s


def process_text(text: Section, game_symbols: GameSymbols):
    block = TextBlock(text.name, [], text.size, thumb=True)
    offset = 0
    while offset < text.size:
        if offset in text.symbols:
            assert offset == 0
            symbol = text.symbols[offset]
            assert symbol.type == Symbol.TYPE_FUNCTION
            block.name = symbol.name
            game_function = game_symbols.get_function(symbol.name)
            if game_function:
                block.location = game_function['value']
                block.space = game_function['size']
        if offset in text.relocations:
            relocation = text.relocations[offset]
            if relocation.type == 2:
                value_offset = text.word(offset, True)
                value = game_symbols.get_symbol(relocation.target)
                if value:
                    value = value['value']
                block.content.append(Abs32(relocation.target, value_offset, value))
                offset += 4
                continue
            if relocation.type == 10:
                assert text.short(offset) == 0xf7ff and text.short(offset + 2) == 0xfffe
                value = game_symbols.get_function(relocation.target)
                if value:
                    value = value['value']
                block.content.append(BL(relocation.target, value))
                offset += 4
                continue
            assert False
        block.content.append(OP(text.short(offset)))
        offset += 2
    return block


def process_rodata(rodata: Section, game_symbols: GameSymbols, text_symbols):
    block = DataBlock(rodata.name, [], rodata.size)
    offset = 0
    while offset < rodata.size:
        if offset in rodata.symbols:
            assert offset == 0
            symbol = rodata.symbols[offset]
            location = game_symbols.get_symbol(symbol.name)
            if location:
                block.location = location['value']
                block.space = location['size']
            block.name = symbol.name
        if offset in rodata.relocations:
            rel = rodata.relocations[offset]
            assert rel.type == 2
            value_offset = rodata.word(offset, True)
            if rel.target not in text_symbols:
                value = game_symbols.get_symbol(rel.target)
                if value:
                    value = value['value']
            else:
                value = None
            block.content.append(Abs32(rel.target, value_offset, value))
            offset += 4
            continue
        block.content.append(RawData(rodata.data[offset:offset + 1]))
        offset += 1
    return block


def process(elf: ELF, game_symbols: GameSymbols):
    return Patch([
        process_text(text, game_symbols)
        for text in elf.text
    ], [
        process_rodata(rodata, game_symbols, [symbol.name for text in elf.text for symbol in text.symbols.values()])
        for rodata in elf.rodata
    ])
