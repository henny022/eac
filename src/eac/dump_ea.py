import sys

from .ast import *
from .visitor import Visitor


class DumpEA(Visitor):
    def __init__(self, file, filename, allow_unsafe_hooks):
        self.file = file
        self.filename = filename
        self.allow_unsafe_hooks = allow_unsafe_hooks

    def _sanitize_label_name(self, name):
        if name.startswith('.'):
            name = self.filename + name
        return name.replace('.', '_').replace('/', '_')

    def visit_op(self, op: OP):
        print(f'SHORT {op.value:#06x}', file=self.file)

    def visit_bl(self, bl: BL):
        if bl.target_value is None:
            print(f'BL({bl.target_name})', file=self.file)
        else:
            print(f'BL({bl.target_value - 0x08000000:#x})', file=self.file)

    def visit_abs32(self, abs: Abs32):
        t = '' if abs.value_offset is None or abs.value_offset == 0 else f'{abs.value_offset:+#x}'
        if abs.target_value is None:
            print(f'POIN {self._sanitize_label_name(abs.target_name)}{t}', file=self.file)
        else:
            print(f'WORD {abs.target_value:#010x}{t}', file=self.file)

    def visit_rawdata(self, data: RawData):
        print('BYTE ' + ' '.join([f'{v:#04x}' for v in data.data]), file=self.file)

    def visit_datablock(self, block: DataBlock):
        print(file=self.file)
        if block.location:
            print('PUSH', file=self.file)
            print(f'ORG {block.location - 0x08000000:#x}', file=self.file)
        print(f'{self._sanitize_label_name(block.name)}:', file=self.file)
        for c in block.content:
            self.visit(c)
        if block.location:
            location = block.location - 0x08000000
            print(f'PROTECT {location:#x} {location + block.size:#x}', file=self.file)
            print('POP', file=self.file)

    def add_label(self, name, thumb):
        print(file=self.file)
        if thumb:
            print(f'{name}_base:', file=self.file)
            print(f'#define {name} {name}_base|1', file=self.file)
        else:
            print(f'{name}:', file=self.file)

    def add_textblock(self, text: TextBlock):
        print(file=self.file)
        if text.location:
            location = (text.location - 0x08000000) & ~1
            print('PUSH', file=self.file)
            print(f'ORG {location:#x}', file=self.file)
        self.add_label(self._sanitize_label_name(text.name), text.thumb)
        for c in text.content:
            self.visit(c)
        if text.location:
            location = (text.location - 0x08000000) & ~1
            print(f'PROTECT {location:#x} {location + text.space:#x}', file=self.file)
            print('POP', file=self.file)

    def hook_textblock(self, text: TextBlock):
        print(file=self.file)
        self.add_label(self._sanitize_label_name(text.name), text.thumb)
        for c in text.content:
            self.visit(c)
        location = (text.location - 0x08000000) & ~1
        print(file=self.file)
        print('PUSH', file=self.file)
        print(f'ORG {location:#x}', file=self.file)
        veneer_size = 16 if (text.location & ~1) % 4 == 0 else 14
        if text.space < veneer_size:
            if text.space < 8:
                print(f'cannot hook function {text.name}, too small', file=sys.stderr)
                exit(1)
            if self.allow_unsafe_hooks:
                print(f'jumpToHack({text.name})', file=self.file)
            else:
                print(f'cannot hook function {text.name}, requires unsafe hook', file=sys.stderr)
                exit(1)
        else:
            print(f'veneer({text.name})', file=self.file)
        print(f'PROTECT {location:#x} {location + text.space:#x}', file=self.file)
        print('POP', file=self.file)

    def visit_textblock(self, text: TextBlock):
        if text.location is None:
            return self.add_textblock(text)
        if text.size <= text.space:
            return self.add_textblock(text)
        return self.hook_textblock(text)
