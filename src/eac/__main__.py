import argparse
import sys

from eac import compile, extract_symbols, dump_symbols


def parse_args(argv):
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(title='subcommands', dest='subcommand', required=True)

    extract_parser = subparsers.add_parser(
        'extract_symbols',
        aliases=['extract', 'e'],
        help='extract game symbols from a game elf binary',
    )
    extract_parser.set_defaults(func=extract_symbols.main)
    extract_parser.add_argument(
        'input',
        help='input game elf binary',
    )
    extract_parser.add_argument(
        '-o', '--output',
        required=True,
        help='output game symbol file',
    )

    compile_parser = subparsers.add_parser(
        'compile',
        aliases=['c'],
        help='compile an elf object file into a patch file'
    )
    compile_parser.set_defaults(func=compile.main)
    compile_parser.add_argument(
        'input',
        help='input elf object file',
    )
    compile_parser.add_argument(
        '-o', '--output',
        required=True,
        help='output patch file',
    )
    compile_parser.add_argument(
        '--symbols',
        required=True,
        help='game symbol file. use eac extract_symbols to create from elf',
    )
    compile_parser.add_argument(
        '--allow-unsafe-hooks',
        action='store_true',
        help='Allow the use of jumpToHack instead of veneer for small functions.\n'
             'This is only safe for functions with 3 or less parameters, so be careful when using this.',
    )
    compile_parser.add_argument(
        '--standalone',
        action='store_true',
        help='Generate standalone patch'
    )

    dump_parser = subparsers.add_parser(
        'dump_symbols',
        aliases=['dump', 'd'],
        help='dump symbols into a patch file'
    )
    dump_parser.set_defaults(func=dump_symbols.main)
    dump_parser.add_argument(
        '-o', '--output',
        required=True,
        help='output patch file',
    )
    dump_parser.add_argument(
        '--symbols',
        required=True,
        help='game symbol file. use eac extract_symbols to create from elf',
    )

    return parser.parse_args(argv)


def main(argv=None):
    args = parse_args(argv or sys.argv[1:])
    args.func(args)


if __name__ == '__main__':
    main()
