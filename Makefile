# === begin config block ===
# change these variables to configure build
REPO := /home/henny/git/zelda/tmc/master
WARNINGS := -Wall -Wextra -Werror
CFLAGS := -O2 -g -fno-inline
# ==== end config block ====

CC := arm-none-eabi-gcc
OUT_DIR := out
SYM_DIR := symbols

CFLAGS += $(WARNINGS) -Wno-multichar -Wno-builtin-declaration-mismatch
CFLAGS += -ffunction-sections -fdata-sections
CFLAGS += -mcpu=arm7tdmi -mthumb -mlong-calls
CFLAGS += -I $(REPO)/tools/agbcc/include -I $(REPO)/include -nostdinc
CFLAGS += -undef -DEU -DREVISION=0 -DLANGUAGE=ENGLISH

.PHONY: run debug dump clean tidy
run: hack.gba
	mgba-qt $<
debug: hack.gba
	mgba-qt -g $<
dump: hack.gba
	arm-none-eabi-objdump -D -bbinary -marmv4t -Mforce-thumb --start-address=0xEF3340 --stop-address=0xEF3460 hack.gba
clean:
	rm -f hack.gba
	rm -rf $(OUT_DIR)
tidy: clean
	rm -rf $(SYM_DIR)

$(OUT_DIR):
	mkdir -p $(OUT_DIR)

$(SYM_DIR):
	mkdir -p $(SYM_DIR)

.SECONDARY:
.DELETE_ON_ERROR:
hack.gba: Buildfile.event $(OUT_DIR)/test.event
	cp tmc_eu.gba hack.gba
	mono ColorzCore.exe A FE8 -output:$@ -input:$<

%.gba: $(OUT_DIR)/%_Buildfile.event
	cp tmc_eu.gba $@
	mono ColorzCore.exe A FE8 -output:$@ -input:$<

$(SYM_DIR)/eu_symbols.json: $(REPO)/tmc_eu.elf | $(SYM_DIR)
	eac extract_symbols -o $@ $<

$(OUT_DIR)/%.event: $(OUT_DIR)/%.o $(SYM_DIR)/eu_symbols.json | $(OUT_DIR)
	eac compile -o $@ --symbols $(SYM_DIR)/eu_symbols.json $<

$(OUT_DIR)/%_Buildfile.event: $(OUT_DIR)/%.o $(SYM_DIR)/eu_symbols.json | $(OUT_DIR)
	eac compile -o $@ --symbols $(SYM_DIR)/eu_symbols.json --standalone $<

$(OUT_DIR)/%.o: %.c | $(OUT_DIR)
	$(CC) $(CFLAGS) -o $@ -c $<

$(OUT_DIR)/%.s: %.c | $(OUT_DIR)
	$(CC) $(CFLAGS) -o $@ -S $<
